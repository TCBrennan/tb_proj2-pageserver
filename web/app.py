from flask import Flask, render_template, request

app = Flask(__name__)

#Index page
@app.route("/")
def index():
    return render_template('index.html')
    
#Main routing. This takes in all searches. The "/<path:name>" is changing the request
#from having multiple / to only 1. I have yet figured out the solution.
@app.route("/<path:name>")
def page_request(name):
	'''
	requests are sent in and first checked for '~','..','//' the '//' doesn't work, see above.
	Renders 403 id illegal characters
	Then will check if the request ends with 'html' or 'css' 
	'''
	if name[:2]=='//' or name[:1]=='~' or name[:2]=='..':
		return render_template('403.html'), 403
	
	elif name.endswith('html') or name.endswith('css'):
		try:
			return render_template(str(name))
		except:
			return render_template('404.html'), 404
   
	else:
		return render_template('401.html'), 401
	#seaches with file types not implimented will give 401

@app.errorhandler(404)
def errorcode(error):
	return render_template('404.html'), 404


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')