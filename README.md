# README #

## Author: Taylor C. Brennan, tbrennan@uoregon.edu ##


## Purpose ##
This is a simple pageserver designed to recieve a request and preform it. It use flask. All request are first validated for illegal characters. If it passes, it then attemps to access the file. It will either render the template, or send an 404 error. Any requests for file types that aren't implimented will get a 401 error.

##Original##
-The pageserver is fully created, but all requests are sent to a picture of a cat. There are no file validation or paths for proper pages. This only handles "GET" requests

##Update - 2020/04/23##
-Still only handles "GET" requests
-When a request comes in:
  -Check for illeagal characters and sends propper error.
  -Error looking for '//'.
  -validate the request is the proper filetype and that it exists. Also gives the proper path for the file. Otherwise sends proper error.


# Tasks

* The goal of this project is to implement the same "file checking" logic that you implemented in project 1 using flask. 

* Like project 1, if a file ("name.html") exists, transmit "200/OK" header followed by that file html. If the file doesn't exist, transmit an error code in the header along with the appropriate page html in the body. You'll do this by creating error handlers taught in class (refer to the slides; it's got all the tricks needed). You'll also create the following two html files with the error messages. 
    * "404.html" will display "File not found!"
    * "403.html" will display "File is forbidden!"
    
* Update your name and email in the Dockerfile.

* You will submit your credentials.ini in canvas. It should have information on how we should get your Dockerfile and your git repo. 
